import React from "react";
import GridMascotas from "./GridMascotas";
// import Sidebar from "../Sidebar";
// import Estilo from "./Grid.css";

const Mascotas = () => {
  const tituloEstilos = {
    fontSize: "24px",
    textAlign: "center",
    margin: "20px 0",
  };
  return (
    <>
      <div style={tituloEstilos}>Mascotas</div>
      <GridMascotas />
    </>
  );
};
export default Mascotas;
