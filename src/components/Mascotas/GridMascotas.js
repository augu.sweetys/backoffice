import React, { useState, useEffect } from "react";
import { Table, Button, FormControl } from "react-bootstrap";
import { Link } from "react-router-dom";

const MascotasTable = () => {
  const [mascotas, setMascotas] = useState([]);
  const [mostrarActivas, setMostrarActivas] = useState(true);
  const [filtro, setFiltro] = useState(""); // Estado para el término de búsqueda

  useEffect(() => {
    fetch("https://645196b0a3221969116631da.mockapi.io/api/Pets")
      .then((response) => {
        if (!response.ok) {
          throw new Error("Error de red: " + response.status);
        }
        return response.json();
      })
      .then((data) => {
        setMascotas(data);
      })
      .catch((error) => {
        console.error("Error de solicitud:", error);
      });
  }, []);

  const handleCheckboxChange = (mascota) => {
    const updatedMascota = {
      ...mascota,
      active: !mascota.active,
      otroColor: "",
    };

    fetch(
      `https://645196b0a3221969116631da.mockapi.io/api/Pets/${mascota.id}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(updatedMascota),
      }
    )
      .then((response) => {
        if (!response.ok) {
        }
        return response.json();
      })
      .then((updatedData) => {
        setMascotas((prevMascotas) =>
          prevMascotas.map((prevMascota) =>
            prevMascota.id === updatedData.id ? updatedData : prevMascota
          )
        );
      })
      .catch((error) => {
        console.error("Error al actualizar el estado 'active':", error);
      });
  };

  const toggleMostrarTodasLasMascotas = () => {
    setMostrarActivas((prevMostrarActivas) => !prevMostrarActivas);
  };

  // Filtra las mascotas en función del término de búsqueda y de si están activas
  const mascotasFiltradas = mascotas
    .filter((mascota) =>
      mostrarActivas ? mascota.active : true
    )
    .filter((mascota) =>
      mascota.breed.toLowerCase().includes(filtro.toLowerCase())
    );

  const renderMascotas = () => {
    return mascotasFiltradas.map((mascota) => (
      <tr key={mascota.id}>
        <th>{mascota.id}</th>
        <td>{mascota.type}</td>
        <td>{mascota.breed}</td>
        <td>
          {Array.isArray(mascota.color)
            ? mascota.color.join(", ")
            : mascota.color}
        </td>
        <td>{mascota.size}</td>
        <td>{mascota.fur}</td>
        <td>
          <input
            type="checkbox"
            className="btn-warning"
            checked={mascota.active}
            onChange={() => handleCheckboxChange(mascota)}
          />
        </td>
        <td>
          <Link to={`/Agregar`}>
            <button>
              <i className="fa fa-pencil">editar</i>{" "}
              {/* Agrega el ícono de lápiz */}
            </button>
          </Link>
        </td>
      </tr>
    ));
  };

  return (
    <div>
      <div className="row justify-content-center mb-3">
        <div className="col-sm-3 text-center">
          <Button
            variant="primary"
            size="sm"
            onClick={toggleMostrarTodasLasMascotas}
          >
            {mostrarActivas ? "Mostrar Todo" : "Mostrar Activas"}
          </Button>
        </div>
        <div className="col-sm-3 text-center">
          <Link to="/Agregar">
            <Button variant="success" size="sm">
              Crear Mascota 🐶🐱
            </Button>
          </Link>
        </div>
      </div>
      
      {/* Agregar el campo de entrada de texto para buscar */}
      <div className="text-center mb-3">
        <FormControl
          type="text"
          placeholder="Buscar por raza"
          onChange={(e) => setFiltro(e.target.value)}
        />
      </div>

      <Table striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Tipo</th>
            <th>Raza</th>
            <th>Color</th>
            <th>Tamaño</th>
            <th>Pelaje</th>
            <th>Mostrar?</th>
          </tr>
        </thead>
        <tbody>{renderMascotas()}</tbody>
      </Table>
    </div>
  );
};

export default MascotasTable;
