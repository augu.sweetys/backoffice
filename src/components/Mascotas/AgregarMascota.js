import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import "./Grid.css";
import { Form, FormGroup, Label, Input, Col, FormFeedback } from "reactstrap";

const AgregarMascota = ({ mascotaToEdit }) => {
  const navigate = useNavigate();
  const isEditing = !!mascotaToEdit;
  console.log(JSON.stringify(mascotaToEdit));
  const [mascota, setMascota] = useState(
    isEditing
      ? mascotaToEdit
      : {
          type: "Perro",
          breed: "",
          size: "Pequeño",
          color: [],
          otroColor: "",
          fur: "Corto",
          active: false,
        }
  );

  const [formErrors, setFormErrors] = useState({
    breed: false,
    color: false,
  });

  const handleInputChange = (e) => {
    const { name, value, type, checked, options, multiple } = e.target;
    const inputValue = type === "checkbox" ? checked : value;
    if (name === "color" && multiple) {
      const selectedOptions = Array.from(options)
        .filter((option) => option.selected)
        .map((option) => option.value);
      setMascota({ ...mascota, [name]: selectedOptions });
    } else {
      setMascota({ ...mascota, [name]: inputValue });
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // Realiza la validación
    const errors = {};

    if (!mascota.breed) {
      errors.breed = true;
    }

    if (mascota.color.length === 0) {
      errors.color = true;
    }

    setFormErrors(errors);

    // Verifica si hay errores
    if (Object.values(errors).some((error) => error)) {
      return;
    }

    // Ahora el objeto `mascota` contiene todos los datos del formulario
    console.log("Datos de la mascota:", mascota);
    if (mascota.color.includes("Otro") && mascota.otroColor) {
      mascota.color = [mascota.otroColor];
    }
    if (mascota.color.length === 0) {
      mascota.color = ["Blanco"];
    }

    // En lugar de hacer una solicitud POST, puedes hacer una solicitud PUT para actualizar la mascota
    // Si no estás editando, utiliza una solicitud POST para crear una nueva mascota

    const url = isEditing
      ? `https://645196b0a3221969116631da.mockapi.io/api/Pets/${mascota.id}`
      : "https://645196b0a3221969116631da.mockapi.io/api/Pets";

    const method = isEditing ? "PUT" : "POST";

    fetch(url, {
      method: method,
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(mascota),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Respuesta del servidor");
        navigate("/Mascotas");
      })
      .catch((error) => {
        console.error("Error al realizar la solicitud:", error);
      });
  };

  return (
    <>
      <div id="tituloEstilos">
        {isEditing ? "Editar Mascota" : "Agregar Mascota"}
      </div>
      <Form>
        <FormGroup row>
          <Label for="type" sm={4}>
            Tipo
          </Label>
          <Col sm={8}>
            <Input
              id="type"
              name="type"
              type="select"
              onChange={handleInputChange}
              disabled={isEditing}
            >
              <option>Perro</option>
              <option>Gato</option>
            </Input>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="breed" sm={4}>
            Raza
          </Label>
          <Col sm={8}>
            <Input
              id="breed"
              name="breed"
              placeholder="Escriba la nueva raza"
              type="search"
              value={mascota.breed}
              onChange={handleInputChange}
              invalid={formErrors.breed}
            />
            <FormFeedback>Debe ingresar el nombre de la raza</FormFeedback>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="size" sm={4}>
            Tamaño
          </Label>
          <Col sm={8}>
            <Input
              id="size"
              name="size"
              type="select"
              value={mascota.size}
              onChange={handleInputChange}
            >
              <option>Pequeño</option>
              <option>Mediano</option>
              <option>Grande</option>
            </Input>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="color" sm={4}>
            Color
          </Label>
          <Col sm={8}>
            <Input
              id="color"
              multiple
              name="color"
              type="select"
              onChange={handleInputChange}
              invalid={formErrors.color}
            >
              <option>Negro</option>
              <option>Blanco</option>
              <option>Dorado</option>
              <option>Marrón</option>
              <option>Gris</option>
              <option>Otro</option>
            </Input>
            <FormFeedback>Seleccione al menos 1 campo</FormFeedback>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="otroColor" sm={4}></Label>
          <Col sm={8}>
            <Input
              id="otroColor"
              name="otroColor"
              placeholder="Especifique el color si seleccionó otro"
              type="text"
              value={mascota.otroColor}
              onChange={handleInputChange}
            />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="fur" sm={4}>
            Pelaje
          </Label>
          <Col sm={8}>
            <Input
              id="fur"
              name="fur"
              type="select"
              value={mascota.fur}
              onChange={handleInputChange}
            >
              <option>Corto</option>
              <option>Mediano</option>
              <option>Largo</option>
            </Input>
          </Col>
        </FormGroup>
        <FormGroup row className="align-items-center">
          <Label for="checkbox" sm={{ size: 4 }}>
            Mostrar?
          </Label>
          <Col sm={8}>
            <Input
              id="checkbox"
              name="active"
              type="checkbox"
              checked={mascota.active}
              onChange={handleInputChange}
            />
          </Col>
        </FormGroup>
        <FormGroup check row>
          <Col sm={{ offset: 4, size: 8 }}>
            <button onClick={handleSubmit}>
              {isEditing ? "Guardar Cambios" : "Crear Mascota"}
            </button>
          </Col>
        </FormGroup>
      </Form>
    </>
  );
};

export default AgregarMascota;
