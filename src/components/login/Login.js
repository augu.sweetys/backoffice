import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import "./Login.css";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    // Realiza la comprobación de campos vacíos aquí
    if (email.trim() === "" || password.trim() === "") {
      alert("Por favor, completa ambos campos.");
      return; // No continuar si hay campos vacíos
    }

    // Aquí puedes agregar la lógica para enviar la información de inicio de sesión al servidor si es necesario.
    console.log("Email:", email);
    console.log("Contraseña:", password);

    // Si los campos no están vacíos, redirige al usuario a /Home
    navigate("/Home");
  };

  return (
    <div className="login-container" style={{ marginTop: "8%" }}>
      <form onSubmit={handleSubmit}>
        <h2>Iniciar sesión</h2>
        <div className="input-container">
          <label>Email:</label>
          <input type="email" value={email} onChange={handleEmailChange} />
        </div>
        <div className="input-container">
          <label>Contraseña:</label>
          <input
            type="password"
            value={password}
            onChange={handlePasswordChange}
          />
        </div>
        <button type="submit">Iniciar sesión</button>
      </form>
    </div>
  );
}

export default Login;
