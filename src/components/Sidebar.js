// src/components/Sidebar.js
import React from "react";
import { NavLink, useNavigate, useLocation } from "react-router-dom";
import * as FaIcons from "react-icons/fa";
import "../bootstrap.min.css";

const Sidebar = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const isLoginRoute = location.pathname === "/Login";
  return (
    <div className="sidebar hidden">
      <div className="image-container">
        <img
          src="https://i.imgur.com/W2JepsE.png"
          alt="Descripción de la imagen"
        />
      </div>
      <ul>
        <li>
          <NavLink
            to="/Home"
            exact
            activeClassName="active"
            className="text-warning rounded py-2 w-100 d-inline-block px-3"
          >
            <FaIcons.FaHome className="me-3" />
            Inicio
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/Mascotas"
            exact
            activeClassName="active"
            className="text-light rounded py-2 w-100 d-inline-block px-3"
          >
            <FaIcons.FaDog className="me-3" />
            Mascotas
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/Usuarios "
            exact
            activeClassName="active"
            className="text-light rounded py-2 w-100 d-inline-block px-3"
          >
            <FaIcons.FaUsers className="me-3" />
            Usuarios
          </NavLink>
        </li>
      </ul>
    </div>
  );
};

export default Sidebar;
