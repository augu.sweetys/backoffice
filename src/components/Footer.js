import React from "react";
import { MDBFooter, MDBContainer } from "mdb-react-ui-kit";

const Footer = () => {
  return (
    <MDBFooter className="text-white footer">
      {" "}
      {/* Agrega la clase "footer" aquí */}
      <MDBContainer className="p-0"></MDBContainer>
      <div
        className="text-center p-1"
        style={{ backgroundColor: "rgba(0, 0, 0, 1)" }}
      >
        © 2023 Copyright:{" "}
        <a className="text-white" href="https://google.com">
          {" "}
          Sweetys.com
        </a>
      </div>
    </MDBFooter>
  );
};

export default Footer;
