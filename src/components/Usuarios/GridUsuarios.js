import React from "react";
import { Table } from "react-bootstrap";

const usuarios = [
  {
    id: 1,
    nombre: "Juan",
    apellido: "Pérez",
    ciudad: "Buenos Aires",
    plan: "rango plata",
    mascotas: ["Max", "Luna", "Buddy"],
  },
  {
    id: 2,
    nombre: "María",
    apellido: "González",
    ciudad: "Madrid",
    plan: "rango oro",
    mascotas: ["Rocky", "Bella"],
  },
  {
    id: 3,
    nombre: "Carlos",
    apellido: "Rodríguez",
    ciudad: "México City",
    plan: "rango diamante",
    mascotas: ["Charlie", "Daisy", "Lucy"],
  },
  {
    id: 4,
    nombre: "Laura",
    apellido: "Martínez",
    ciudad: "Bogotá",
    plan: "rango oro",
    mascotas: ["Coco", "Bobby"],
  },
  {
    id: 5,
    nombre: "Pedro",
    apellido: "López",
    ciudad: "Santiago",
    plan: "rango plata",
    mascotas: ["Beauty", "Simba"],
  },
  {
    id: 6,
    nombre: "Ana",
    apellido: "Sánchez",
    ciudad: "Lima",
    plan: "rango diamante",
    mascotas: ["Lucky", "Toby"],
  },
  {
    id: 7,
    nombre: "Diego",
    apellido: "Hernández",
    ciudad: "Montevideo",
    plan: "rango plata",
    mascotas: ["Zeus", "Rex"],
  },
  {
    id: 8,
    nombre: "Sofía",
    apellido: "Ramírez",
    ciudad: "Barcelona",
    plan: "rango oro",
    mascotas: ["Puppa", "Sol", "Rocky"],
  },
  {
    id: 9,
    nombre: "Javier",
    apellido: "Torres",
    ciudad: "Lisboa",
    plan: "rango diamante",
    mascotas: ["Max", "Rusty", "Herman"],
  },
  {
    id: 10,
    nombre: "Marcela",
    apellido: "Fernández",
    ciudad: "Sao Paulo",
    plan: "rango oro",
    mascotas: ["Kimmi", "Lola"],
  },
  {
    id: 11,
    nombre: "Juan",
    apellido: "Pérez",
    ciudad: "Buenos Aires",
    plan: "rango plata",
    mascotas: ["Max", "Luna", "Buddy"],
  },
  {
    id: 12,
    nombre: "María",
    apellido: "González",
    ciudad: "Madrid",
    plan: "rango oro",
    mascotas: ["Rocky", "Bella"],
  },
  {
    id: 13,
    nombre: "Carlos",
    apellido: "Rodríguez",
    ciudad: "México City",
    plan: "rango diamante",
    mascotas: ["Charlie", "Daisy", "Lucy"],
  },
  {
    id: 14,
    nombre: "Laura",
    apellido: "Martínez",
    ciudad: "Bogotá",
    plan: "rango oro",
    mascotas: ["Coco", "Bobby"],
  },
  {
    id: 15,
    nombre: "Pedro",
    apellido: "López",
    ciudad: "Santiago",
    plan: "rango plata",
    mascotas: ["Beauty", "Simba"],
  },
  {
    id: 16,
    nombre: "Ana",
    apellido: "Sánchez",
    ciudad: "Lima",
    plan: "rango diamante",
    mascotas: ["Lucky", "Toby"],
  },
  {
    id: 17,
    nombre: "Diego",
    apellido: "Hernández",
    ciudad: "Montevideo",
    plan: "rango plata",
    mascotas: ["Zeus", "Rex"],
  },
  {
    id: 18,
    nombre: "Sofía",
    apellido: "Ramírez",
    ciudad: "Barcelona",
    plan: "rango oro",
    mascotas: ["Puppa", "Sol", "Rocky"],
  },
  {
    id: 19,
    nombre: "Javier",
    apellido: "Torres",
    ciudad: "Lisboa",
    plan: "rango diamante",
    mascotas: ["Max", "Rusty", "Herman"],
  },
  {
    id: 20,
    nombre: "Marcela",
    apellido: "Fernández",
    ciudad: "Sao Paulo",
    plan: "rango oro",
    mascotas: ["Kimmi", "Lola"],
  },
  // Puedes agregar más registros si lo deseas
];


const UsuariosTable = () => {
  const renderUsuarios = () => {
    return usuarios.map((usuario) => (
      <tr key={usuario.id}>
        <th>{usuario.id}</th>
        <td>{usuario.nombre}</td>
        <td>{usuario.apellido}</td>
        <td>{usuario.ciudad}</td>
        <td>{usuario.mascotas.join(", ")}</td>
        <td>{usuario.plan}</td>
      </tr>
    ));
  };

  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Apellido</th>
          <th>Ciudad</th>
          <th>Mascotas</th>
          <th>Plan</th>
        </tr>
      </thead>
      <tbody>{renderUsuarios()}</tbody>
    </Table>
  );
};

export default UsuariosTable;
