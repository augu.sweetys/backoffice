import React from "react";
import Sidebar from "../Sidebar";
import Gridusuarios from './GridUsuarios'
 
const Usuarios = () => {
  const tituloEstilos = {
    fontSize: "24px", // Tamaño de fuente mayor
    textAlign: "center", // Texto centrado
    margin: "20px 0", // Margen superior e inferior de 20px
    
  };
  return (
    <>
      <div style={tituloEstilos}>Usuarios</div>
      <Gridusuarios />
    </>
  );
};

export default Usuarios;
