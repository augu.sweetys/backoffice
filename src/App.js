import "./App.css";
import Home from "./components/Home";
import Mascotas from "./components/Mascotas/Mascotas";
import Usuarios from "./components/Usuarios/Usuarios";
import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Sidebar from "./components/Sidebar";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Login from "./components/login/Login";
import "bootstrap/dist/css/bootstrap.min.css";
import AgregarMascota from "./components/Mascotas/AgregarMascota";

function App({ datos }) {
  return (
    <Router>
      <div className="app-container">
        <div className="content-container">
          {datos.sidebar && <Sidebar />}
          <div className="content w-100">
            {datos.navbar && <Navbar />}
            <Routes>
              {datos.listaStrings.map((nombre) => (
                <Route
                  key={nombre}
                  path={`/${nombre}`}
                  element={<ComponenteGenerado nombre={nombre} />}
                />
              ))}

              {/* Ruta predeterminada */}
              <Route
                path="/*"
                element={<ComponenteGenerado nombre={datos.listaStrings[0]} />}
              />
              <Route path="/Agregar" element="AgregarMascota" />
            </Routes>
          </div>
        </div>
        <Footer />
      </div>
    </Router>
  );
}

function ComponenteGenerado({ nombre }) {
  // Lógica para generar componentes basados en el nombre de la ruta
  switch (nombre) {
    case "Home":
      return <Home />;
    case "Mascotas":
      return <Mascotas />;
    case "Usuarios":
      return <Usuarios />;
    case "Agregar":
      return <AgregarMascota />;
    case "Login":
      return <Login />;
  }
}

export default App;
