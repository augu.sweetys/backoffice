import React from "react";
// import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom/client";
import "./App.css";
import "./bootstrap.min.css";
import "bootstrap/dist/css/bootstrap.min.css";
import App from "./App";

const root = ReactDOM.createRoot(document.getElementById("root"));
const datos = {
  navbar: true,sidebar: true,listaStrings: ["Home", "Mascotas", "Usuarios", "Agregar"],
  // navbar: false,sidebar: false,listaStrings: ["Login"],
};
root.render(
  <React.StrictMode>
    <App datos={datos} />
  </React.StrictMode>
);
